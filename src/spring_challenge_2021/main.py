# -*- coding: utf-8 -*-
# -*- python3.ci -*-
# -*- blacked.ci -*-

import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.
DAY_SHADOW = {
    0: 0, 1: 1, 2: 2, 3: 3, 4: 4, 5: 5,
    6: 0, 7: 1, 8: 2, 9: 3, 10: 4, 11: 5,
    12: 0, 13: 1, 14: 2, 15: 3, 16: 4, 17: 5,
    18: 0, 19: 1, 20: 2, 21: 3, 22: 4, 23: 5
}
MIN_LVL3_COUNT_FOR_ACCEPT_COMPLETE_ACTION = [
    6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6,
    6, 4, 4, 3, 3, 2,
    2, 2, 0, 0, 0, 0
]
MAX_PER_SIZE = {
    0: 1,
    1: 2,
    2: 3,
    3: 12,
}
cells = {}
indexes_with_shadow = {}


class Cell:
    def __init__(self, index, richness, neigh_0, neigh_1, neigh_2, neigh_3, neigh_4, neigh_5):
        self.index = index
        self.richness = richness
        self.neigh = [neigh_0, neigh_1, neigh_2, neigh_3, neigh_4, neigh_5]

    def get_neigh_with_shadow(self, day):
        # print(day, self.index, self.neigh[DAY_SHADOW.get(day)], file=sys.stderr, flush=True)
        return self.neigh[DAY_SHADOW.get(day)]

    def get_invert_richness(self):
        return 3 - self.richness


class Action():
    def __init__(self, possible_action, trees, day, sun, index_with_shadow, new_day):
        action = possible_action.split()
        self.action_name = action[0]

        if self.action_name != "WAIT":
            self.action_index = int(action[1])
        if self.action_name == 'SEED':
            self.destination_index = int(action[2])

        self.trees = trees
        self.day = day
        self.sun = sun
        self.index_with_shadow = index_with_shadow
        self.is_new_day = new_day

    def _must_not_exec(self, trees_value):
        sun_needed_to_complete = 4 * (trees_value.count(3) + 2)

        return (
                not self.action_name
                or "WAIT" == self.action_name
                or self.day == 0
                or (self.day == 22 and self.sun < sun_needed_to_complete)
        )

    def _get_index_to_seed(self, trees_value):
        trees_with_size_lt_3 = sum(v < 3 for v in self.trees.values())
        if (
                trees_value.count(0) < MAX_PER_SIZE.get(0)
                and (
                1 < self.day < 17
                or (self.day == 18 and trees_with_size_lt_3 < 2)
        )
        ):
            shadow_priority = '1' if self.destination_index not in self.index_with_shadow else f"1{self.index_with_shadow.count(self.destination_index)}"
            invert_richness = cells.get(self.action_index).get_invert_richness()
            return int(f'{shadow_priority}{invert_richness}{self.destination_index:02d}{self.action_index:02d}')

    def _get_index_to_complete(self, trees_value):
        if trees_value.count(3) >= MIN_LVL3_COUNT_FOR_ACCEPT_COMPLETE_ACTION[day]:
            shadow_priority = '1' if self.action_index in self.index_with_shadow else '2'
            return int(f'{shadow_priority}{self.action_index:02d}')

    def _get_index_to_grow(self, trees_value):
        actual_tree_size = self.trees.get(self.action_index)
        next_tree_size = int(actual_tree_size + 1)

        if actual_tree_size == 2 and self.day < 23 and not self.is_new_day:
            return int(f'{self.action_index}')

        if trees_value.count(next_tree_size) < MAX_PER_SIZE.get(next_tree_size):
            invert_size = 3 - self.trees.get(self.action_index)
            return int(f'1{invert_size}{self.action_index:02d}')

    def get_index_if_can_exec(self):
        trees_value = list(self.trees.values())

        if self._must_not_exec(trees_value):
            return

        if self.action_name == "COMPLETE":
            return self._get_index_to_complete(trees_value)

        if self.day == 23:
            return

        if self.action_name == "GROW":
            return self._get_index_to_grow(trees_value)

        if self.action_name == "SEED":
            return self._get_index_to_seed(trees_value)


number_of_cells = int(input())  # 37
for i in range(number_of_cells):
    # index: 0 is the center cell, the next cells spiral outwards
    # richness: 0 if the cell is unusable, 1-3 for usable cells
    # neigh_0: the index of the neighbouring cell for each direction
    index, richness, neigh_0, neigh_1, neigh_2, neigh_3, neigh_4, neigh_5 = [int(j) for j in input().split()]
    # print(index, richness, neigh_0, neigh_1, neigh_2, neigh_3, neigh_4, neigh_5, file=sys.stderr, flush=True)
    cells[index] = Cell(index, richness, neigh_0, neigh_1, neigh_2, neigh_3, neigh_4, neigh_5)


def set_indexes_with_shadow(day, new_day, cell_index, size):
    for n_day in (range(day, day + 6) if day < 19 and new_day else []):
        if n_day > 19:
            break
        index_with_shadow = cell_index

        for i in range(size):
            index_with_shadow = cells.get(index_with_shadow).get_neigh_with_shadow(n_day)
            if index_with_shadow == -1:
                break
            indexes_with_shadow[day].append(index_with_shadow)


def is_new_day():
    new_day = (day not in indexes_with_shadow)
    if day not in indexes_with_shadow:
        indexes_with_shadow[day] = []
    return new_day


# game loop
while True:
    day = int(input())  # the game lasts 24 days: 0-23
    nutrients = int(input())  # the base score you gain from the next COMPLETE action
    # sun: your sun points; score: your current score
    sun, score = [int(i) for i in input().split()]
    inputs = input().split()
    opp_sun = int(inputs[0])  # opponent's sun points
    opp_score = int(inputs[1])  # opponent's score
    opp_is_waiting = inputs[2] != "0"  # whether your opponent is asleep until the next day
    number_of_trees = int(input())  # the current amount of trees
    # print(day, nutrients, sun, score, opp_sun, opp_score, opp_is_waiting, number_of_trees, file=sys.stderr, flush=True)

    trees = {}
    new_day = is_new_day()

    for i in range(number_of_trees):
        inputs = input().split()
        cell_index = int(inputs[0])  # location of this tree
        size = int(inputs[1])  # size of this tree: 0-3
        is_mine = inputs[2] != "0"  # 1 if this is your tree
        is_dormant = inputs[3] != "0"  # 1 if this tree is dormant

        # print(cell_index, size, is_mine, is_dormant, file=sys.stderr, flush=True)
        set_indexes_with_shadow(day, new_day, cell_index, size)

        if is_mine:
            trees[cell_index] = size

    print(nutrients, file=sys.stderr, flush=True)

    print(indexes_with_shadow, file=sys.stderr, flush=True)

    number_of_possible_actions = int(input())  # all legal actions

    print(trees, file=sys.stderr, flush=True)

    actions = {}
    for i in range(number_of_possible_actions):
        possible_action = input()  # try printing something from here to start with

        new_action = Action(possible_action, trees, day, sun, indexes_with_shadow[day], new_day)
        new_action_index = new_action.get_index_if_can_exec()
        if new_action_index:
            actions[new_action_index] = possible_action

        print(possible_action, file=sys.stderr, flush=True)

    print(actions, file=sys.stderr, flush=True)

    if actions:
        print(actions[sorted(actions)[0]])
    else:
        print("WAIT")

    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr, flush=True)

    # GROW cellIdx | SEED sourceIdx targetIdx | COMPLETE cellIdx | WAIT <message>
    # print("WAIT")
