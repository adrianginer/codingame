# -*- coding: utf-8 -*-
# -*- python3.ci -*-
# -*- blacked.ci -*-

"""
Como desarrollador en Roche, su trabajo puede tener un impacto directo en el futuro de la ciencia, la atención médica y la calidad de vida de los pacientes de todo el mundo.

En este juego, un complejo de Roche está equipado con un dron robótico de última generación capaz de interactuar con todo tipo de software, desde el software utilizado para diagnosticar enfermedades a nivel molecular hasta el descubrimiento de nuevos medicamentos.

Sin embargo, el robot necesita un algoritmo para optimizar este proceso. Y para eso, Roche te necesita. (tenga en cuenta que este es un juego y no refleja la realidad)

 	Nota
Code4Life es un concurso especial. No se sorprenda ni se desanime si tarda más de lo habitual en tener su primera IA funcionando; ¡es totalmente normal! Puede ver esta primera parte como la resolución de un rompecabezas mediano. Entonces, es un juego clásico de liga multijugador.
 	La meta
Produzca medicamentos y maximice su puntaje transportando artículos a través de un complejo médico.
 	Reglas
Se juega un juego con 2 jugadores. Cada jugador controla un robot.

El complejo está compuesto por 3 módulos denominadosDIAGNÓSTICO, MOLÉCULAS y LABORATORIO.
Los robots pueden transferir dos tipos de elementos desde y hacia los módulos: archivos de datos de muestra y moléculas .

En pocas palabras, debe optimizar los movimientos de su robot para:
Recopile archivos de datos de muestra de la nube en elDIAGNÓSTICO módulo.
Reúna las moléculas necesarias para los medicamentos en elMOLÉCULAS módulo.
Producir los medicamentos en elLABORATORIO módulo y recoger sus puntos de salud.

 	Detalles
Los robots
Cada jugador tiene un robot. Ambos robots tienen la misma posición inicial.
Un robot puede transportar hasta 3 archivos de datos de muestra y10 moléculas .
Un jugador puede mover su robot de un módulo a otro por medio del Módulo GOTO mando.
Una vez que el robot está en la interfaz de un módulo, puede conectarse a él con elCONECTARmando. Esto tendrá un efecto diferente para cada módulo.
Data de muestra
Un archivo de datos de muestra es un elemento que representa todos los datos conocidos en una muestra de tejido recolectada de un paciente no tratado. La investigación de esta muestra puede conducir en última instancia a la producción de medicamentos para prolongar la vida de todos los pacientes con la misma dolencia.
Los datos de una muestra se asocian con la lista de moléculas necesarias para producir el medicamento para esa muestra.
Un dato de muestra le otorgará un número variable de puntos de vida cuando lo investigue.
Moléculas
Una molécula puede ser de cinco tipos: A, B, C, re o ES.
 	Módulos

La máquina de diagnóstico que se

conecta a este módulo conID DE CONEXIÓN transferirá el archivo de datos de muestra identificado por carné de identidaddesde la nube de la máquina de diagnóstico a su robot.

El módulo de distribución de moléculas
Conectando a este módulo con CONECTAR tipo, dónde tipo es uno de los tipos de moléculas, transferirá una molécula disponible a su robot del tipo deseado.
El módulo de laboratorio
Para utilizar este módulo, el robot del jugador debe llevar un archivo de datos de muestra , así como la cantidad necesaria de moléculas para producir el medicamento de esa muestra.
Conectando a este módulo con ID DE CONEXIÓN dónde carné de identidades el identificador de una muestra de datos que el jugador puede buscar, tendrá varios efectos:
Los datos de muestra carné de identidad así como las moléculas asociadas se eliminan del juego.
Los jugadores obtienen tantos puntos como los puntos de salud de la muestra.
El jugador adquiere experiencia en moléculas : el robot necesitará 1 molécula menos del tipo especificado por la muestra para producir todos los medicamentos posteriores.

Condiciones de victoria
Tu robot ha obtenido la mayor cantidad de puntos de vida después de 200 turnos o una vez que un jugador tiene al menos 170 puntos.

Perder condiciones
No proporciona una salida válida.
 	Entrada de juego
Entrada de inicialización
Línea 1: siempre 0. Ignore para esta liga y no quite para las siguientes ligas.
Entrada para un turno de juego
Para cada jugador, 1 línea: 1 cadena seguida de 12 enteros (siempre eres el primer jugador):
objetivo: módulo donde se encuentra el jugador.
y: ignorar para esta liga.
Puntuación: el número de puntos de vida del jugador
almacenamientoA, almacenamientoB, almacenamientoC, almacenamientoD, almacenamientoE: número de moléculas que tiene el jugador para cada tipo de molécula.
expertiseA, periciaB, experienciaC, experienciaD, experienciaE: ignore esto para esta liga.
Línea siguiente:disponibleA, disponibleB, disponible C, disponibleD, disponibleE: número de moléculas disponibles. Ignore para esta liga, siempre hay suficientes moléculas disponibles.

Línea siguiente:sampleCount: el número de muestras que hay actualmente en el juego.
próximosampleCount líneas:
ejemplo de identificacion: identificación única para la muestra.
transportado por: 0 si la muestra la lleva usted, 1 por el otro robot, -1 si la muestra está en la nube.
rango: ignorar para esta liga .
ganancia: ignorar para esta liga .
salud: número de puntos de vida que obtienes con esta muestra.
costA, costB, costC, costD, costE: número de moléculas de cada tipo necesarias para investigar la muestra
Salida para un turno de juego
Cada turno emite uno de los siguientes comandos:
IR módulo: moverse hacia el módulo de destino.
CONECTAR carné de identidad/tipo: conectarse al módulo de destino con la identificación de muestra o el tipo de molécula especificados.
Restricciones
Tiempo de
respuesta para el primer turno ≤ 1000ms Tiempo de respuesta para un turno ≤ 50ms

¿Qué me depara en las ligas superiores?

Las reglas adicionales disponibles en ligas superiores son:
Obtenga muestras no diagnosticadas de un nuevo MUESTRAS módulo.
Las moléculas estarán en suministro limitado.
Al producir medicamentos, obtendrá experiencia que le permitirá producir medicamentos costosos y proyectos científicos completos.
Su robot tardará un tiempo en pasar de un módulo a otro.
"""
