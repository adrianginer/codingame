# -*- coding: utf-8 -*-
# -*- python3.ci -*-
# -*- blacked.ci -*-

import sys
import math


# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.
def debug(*args):
    print(args, file=sys.stderr, flush=True)


class AbstractObject:
    def __init__(self, tier_0, tier_1, tier_2, tier_3, action_id, repeatable):
        self.tier_0 = int(tier_0)
        self.tier_1 = int(tier_1)
        self.tier_2 = int(tier_2)
        self.tier_3 = int(tier_3)
        self.action_id = int(action_id)
        self.repeatable = bool(repeatable)


class Potion(AbstractObject):
    def __init__(
        self,
        tier_0,
        tier_1,
        tier_2,
        tier_3,
        action_id,
        repeatable,
        price,
        tome_index,
        tax_count,
    ):

        self.price = int(price)
        self.tome_index = int(tome_index)
        self.tax_count = int(tax_count)

        super().__init__(tier_0, tier_1, tier_2, tier_3, action_id, repeatable)


class Movement(AbstractObject):
    def __init__(self, tier_0, tier_1, tier_2, tier_3, action_id, repeatable):
        super().__init__(tier_0, tier_1, tier_2, tier_3, action_id, repeatable)


class NewMovement(AbstractObject):
    def __init__(
        self,
        tier_0,
        tier_1,
        tier_2,
        tier_3,
        action_id,
        repeatable,
        price,
        tome_index,
        tax_count,
    ):
        super().__init__(tier_0, tier_1, tier_2, tier_3, action_id, repeatable)

        self.price = int(price)
        self.tome_index = int(tome_index)
        self.tax_count = int(tax_count)
        self.total_ingredients = self._set_total_ingredients()

    def _set_total_ingredients(self):
        return self.tier_0 + self.tier_1 + self.tier_2 + self.tier_3 + self.tax_count


class Inventory:
    def __init__(self, tier_0, tier_1, tier_2, tier_3, score):
        self.tier_0 = int(tier_0)
        self.tier_1 = int(tier_1)
        self.tier_2 = int(tier_2)
        self.tier_3 = int(tier_3)
        self.score = int(score)


def get_potions_and_movements():
    potions = []
    new_movements = []
    movements = []

    for i in range(action_count):
        # action_id: the unique ID of this spell or recipe
        # action_type: CAST, OPPONENT_CAST, LEARN, BREW
        # tier_0: tier-0 ingredient change
        # tier_1: tier-1 ingredient change
        # tier_2: tier-2 ingredient change
        # tier_3: tier-3 ingredient change
        # price: the price in rupees if this is a potion
        # tome_index: the index in the tome if this is a tome spell, equal to the read-ahead tax
        # tax_count: the amount of taxed tier-0 ingredients you gain from learning this spell
        # castable: 1 if this is a castable player spell
        # repeatable: 1 if this is a repeatable player spell
        action_id, action_type, tier_0, tier_1, tier_2, tier_3, price, tome_index, tax_count, castable, repeatable = (
            input().split()
        )

        tome_index = int(tome_index)
        tax_count = int(tax_count)
        castable = castable != "0"
        repeatable = repeatable != "0"

        if action_type == "BREW":
            potions.append(
                Potion(
                    tier_0,
                    tier_1,
                    tier_2,
                    tier_3,
                    action_id,
                    repeatable,
                    price,
                    tome_index,
                    tax_count,
                )
            )
        elif action_type == "CAST" and castable:
            movements.append(
                Movement(tier_0, tier_1, tier_2, tier_3, action_id, repeatable)
            )
        elif action_type == "LEARN":
            new_movements.append(
                NewMovement(
                    tier_0,
                    tier_1,
                    tier_2,
                    tier_3,
                    action_id,
                    repeatable,
                    price,
                    tome_index,
                    tax_count,
                )
            )
        else:
            continue
        debug(
            action_id,
            action_type,
            tier_0,
            tier_1,
            tier_2,
            tier_3,
            price,
            tome_index,
            tax_count,
            castable,
            repeatable,
        )
    return potions, new_movements, movements


def brew_potion(potions, inventory):
    debug("BREW POTION ---------------------------")

    potion_id = None
    for potion in potions:
        debug(potion.__dict__)
        debug(inventory.__dict__)
        debug("---------------------------")

        if (
            inventory.tier_0 >= abs(potion.tier_0)
            and inventory.tier_1 >= abs(potion.tier_1)
            and inventory.tier_2 >= abs(potion.tier_2)
            and inventory.tier_3 >= abs(potion.tier_3)
        ):
            potion_id = potion.action_id
            debug("BREW {}".format(potion_id))
            break

    debug("END BREW POTION {}---------------------------".format(potion_id))

    return potion_id


def can_learn(mov, inv):
    total_0 = mov.tier_0 + inv.tier_0

    if mov.tome_index > inv.tier_0:
        return False

    if mov.total_ingredients < 0:
        return False

    if total_0 > 5:
        return False

    return True


def learn_new_movement(new_movements, inventory):
    debug("LOOKING FOR LEARN NEW MOV ---------------------------")
    movement_id = None

    # new_movements = sorted(new_movements, key=itemgetter('total_ingredients'), reverse=True)
    for new_movement in new_movements:
        debug(new_movement.__dict__)
        if can_learn(new_movement, inventory):
            movement_id = new_movement.action_id
            break

    debug(
        "END LOOKING FOR LEARN NEW MOV {}---------------------------".format(
            movement_id
        )
    )

    return movement_id


def can_mov(movement, inventory, count):
    total_0 = movement.tier_0 * count + inventory.tier_0
    total_1 = movement.tier_1 * count + inventory.tier_1
    total_2 = movement.tier_2 * count + inventory.tier_2
    total_3 = movement.tier_3 * count + inventory.tier_3

    total = total_0 + total_1 + total_2 + total_3

    if not total <= 10:
        return False

    if total_0 < 0 or total_1 < 0 or total_2 < 0 or total_3 < 0:
        return False

    return True


def need_mov(movement, inventory, count):
    need_mov = True

    for i in range(4):
        mov_tier = getattr(movement, "tier_{}".format(i)) * count
        inv_tier = getattr(inventory, "tier_{}".format(i))
        total_tier = mov_tier + inv_tier

        if (total_tier > 4 and count == 1) or (total_tier > 6 and count == 2):
            need_mov = False
            break

    return need_mov


def is_repeatable(movement, inventory):
    if not movement.repeatable:
        return False

    can_do = True
    for i in range(4):
        mov_tier = getattr(movement, "tier_{}".format(i))
        inv_tier = getattr(inventory, "tier_{}".format(i))

        if mov_tier < 0 and abs(mov_tier * 2) > inv_tier:
            can_do = False
            break

    return can_do


def get_next_mov(movements, inventory):
    debug("LOOKING FOR MOV ---------------------------")

    movement_id = None

    for movement in movements:
        debug(movement.__dict__, inventory.__dict__)
        debug("TOTAL---------------------------")

        if is_repeatable(movement, inventory):
            count = 2
        else:
            count = 1

        if can_mov(movement, inventory, count) and need_mov(movement, inventory, count):
            movement_id = movement.action_id
            break

    debug("END LOOKING FOR MOV {}---------------------------".format(movement_id))

    return movement_id, count


if __name__ == "__main__":
    learn_more = True
    # game loop
    while True:
        action_count = int(input())  # the number of spells and recipes in play

        potions, new_movements, movements = get_potions_and_movements()

        for i in range(2):
            # tier_0: tier-0 ingredients in inventory
            # score: amount of rupees
            tier_0, tier_1, tier_2, tier_3, score = [int(j) for j in input().split()]
            if i == 0:
                inventory = Inventory(tier_0, tier_1, tier_2, tier_3, score)

        brew = brew_potion(potions, inventory)
        if brew:
            print("BREW {}".format(brew))
            continue

        if learn_more:
            learn = learn_new_movement(new_movements, inventory)
            if learn:
                if len(movements) >= 10:
                    learn_more = False
                print("LEARN {}".format(learn))
                continue
        elif len(movements) < 8:
            learn_more = True

        cast, count = get_next_mov(movements, inventory)
        if cast:
            print("CAST {} {}".format(cast, count))
            continue

        print("REST")

        # Write an action using print
        # To debug: print("Debug messages...", file=sys.stderr, flush=True)

        # BREW <id> | CAST <id> [<times>] | LEARN <id> | REST | WAIT
