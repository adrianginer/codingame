# -*- coding: utf-8 -*-
# -*- python3.ci -*-
# -*- blacked.ci -*-

import pytest
from src.fall_challenge_2020 import (
    Movement,
    Inventory,
    NewMovement,
    Potion,
    is_repeatable,
    can_mov,
    need_mov,
    can_learn,
)


@pytest.mark.parametrize(
    "movement, inventory, expected_result",
    [
        (Movement(2, 0, 0, 0, 0, False), Inventory(3, 0, 0, 0, 0), False),
        (Movement(-1, 1, 0, 0, 0, True), Inventory(0, 2, 0, 0, 0), False),
        (Movement(-2, 6, 0, 0, 0, True), Inventory(4, 0, 0, 0, 0), True),
        (Movement(-2, 2, 0, 0, 0, True), Inventory(2, 0, 0, 0, 0), False),
        (Movement(0, 3, 0, -1, 0, True), Inventory(4, 0, 1, 2, 0), True),
    ],
)
def test_is_repeatable(movement, inventory, expected_result):
    result = is_repeatable(movement, inventory)
    assert result == expected_result


@pytest.mark.parametrize(
    "movement, inventory, count, expected_result",
    [
        (Movement(-2, 2, 0, 0, 0, True), Inventory(2, 0, 0, 0, 0), 1, True),
        (Movement(0, 3, 0, -1, 0, True), Inventory(4, 0, 1, 2, 0), 2, False),
        (Movement(-2, 3, 0, 0, 0, True), Inventory(4, 1, 0, 0, 0), 2, True),
    ],
)
def test_can_mov(movement, inventory, count, expected_result):
    result = can_mov(movement, inventory, count)
    assert result == expected_result


@pytest.mark.parametrize(
    "movement, inventory, count, expected_result",
    [
        (Movement(-2, 2, 0, 0, 0, True), Inventory(2, 0, 0, 0, 0), 1, True),
        (Movement(-2, 3, 0, 0, 0, True), Inventory(4, 1, 0, 0, 0), 2, False),
    ],
)
def test_need_mov(movement, inventory, count, expected_result):
    result = need_mov(movement, inventory, count)
    assert result == expected_result


@pytest.mark.parametrize(
    "new_movement, inventory, expected_result",
    [
        (NewMovement(0, -3, 0, 2, 0, True, 0, 0, 1), Inventory(3, 0, 0, 0, 0), True),
        (NewMovement(3, -2, 1, 0, 0, True, 0, 0, 1), Inventory(5, 0, 0, 0, 0), False),
    ],
)
def test_can_learn(new_movement, inventory, expected_result):
    result = can_learn(new_movement, inventory)
    assert expected_result == result
